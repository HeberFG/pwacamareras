const login = () => {
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const response = fetch('http://localhost:8000/api/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "email": email,
            "password": password
        })
    }).then((data) => {
        return data.json()
    }).catch((e) => {
        console.error('Hola' + e);
    })
    response.then((jsonData) => {
        if (jsonData.access_token) {
            localStorage.setItem("access_token", jsonData.access_token)
            window.location.href = 'http://localhost:8080/pages/rooms.html'
        } else {
            console.log('Error al iniciar sesión');
        }
    }).catch((e) => {
        console.log(e);
    })
}

const confirmSession = () => {
    if (localStorage.getItem("access_token") == null) {
        window.location.href = 'http://localhost:8080/pages/login.html'
    } else {
        const response = fetch("http://localhost:8000/api/userProfile", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + localStorage.getItem("access_token"),
            },
        })
            .then((data) => {
                return data.json();
            })
            .catch((e) => {
                window.location.href = 'http://localhost:8080/login.html'
            });

        response.then((jsonData) => {
            document.getElementById('usernameNav').innerHTML = jsonData.data.name
            document.getElementById('usernameMenu').innerHTML = jsonData.data.name
        });
    }
}

const getRooms = () => {
    const response = fetch('http://localhost:8000/api/room/getAllRoomByUser', {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
        },
    })
        .then((data) => {
            return data.json();
        })
        .catch((e) => {
            console.error(e);
        });

    response.then((jsonData) => {
        const roomList = document.getElementById('roomList')
        var body = ''
        jsonData.data.forEach(element => {
            getRoomDetailOff(element.id)
            body = body + Card(element)
        });
        roomList.innerHTML = body
    });
}

const goDetail = (id) => {
    localStorage.setItem('room', id)
    window.location.href = 'http://localhost:8080/pages/roomDetail.html'
}

const getRoomDetail = () => {
    const room = localStorage.getItem('room')
    const response = fetch(`http://localhost:8000/api/room/show/${room}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            //'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
        }
    }).then((data) => {
        return data.json();
    })
        .catch((e) => {
            console.error(e);
        });

    response.then((jsonData) => {
        console.log(jsonData.data);
        const container = document.getElementById("roomDet")
        container.innerHTML = RoomDetail(jsonData.data)
    });
}

const getRoomDetailOff = (id) => {
    const response = fetch(`http://localhost:8000/api/room/show/${id}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            //'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
        }
    })
}


const logout = () => {

}

//Components Section

const Card = (element) => {
    return `<div class="col">
    <div class="card mx-auto mb-5" style="width: 18rem">
        <img src="../images/habitacion.png" class="card-img-top" alt="100px" />
        <div class="card-body">
            <h5 class="card-title">Habitación #${element.number}</h5>
            <p class="card-text">
                ${element.type === 'Suit' ? 'Suit amplia con todos los servicios incluidos.' : 'Habitación familiar para disfrutar con toda la familia'}
            </p>
            <ul class="list-group list-group-flush">
            <li class="list-group-item">Edificio: ${element.build.name}  /  Piso:  ${element.floor}</li>
                <li class="list-group-item">Estado: ${element.state === 'Liberada' ? `<span class="badge rounded-pill text-bg-success">${element.state}</span>` : `<span class="badge rounded-pill text-bg-warning">${element.state}</span>`}</li>
                <li class="list-group-item">Tipo: ${element.type}</li>
            </ul>
            <button style="background-color: #637cff; color: white" class="btn mt-3" type="button" onclick="goDetail(${element.id})">Seleccionar</button>
        </div>
    </div>
</div>`
}

const RoomDetail = (element) => {
    /*return`<img src="../images/habitacion.png" class="card-img-top" alt="100px" />
    <h1>Id: ${element.id}</h1>
    <h1>Piso: ${element.floor}</h1>
    <h1>Número: ${element.number}</h1>
    <h1>Edificio: ${element.build.name}</h1>
    <h1>Tipo: ${element.type}</h1>
    <h1>Estado: ${element.state}</h1>`
    */
    return `
    <div class="container pt-5">
    <h2 style="text-align: left"><b>Habitación:</b> ${element.number} </h2>
    <h2 style="text-align: left"><b>Piso:</b>  ${element.floor}</h2>
    <h2 style="text-align: left"><b>Estado:</b> ${element.state}</h2>
    <div style="text-align: center" class="mt-5">
        <h5>Selecciona el estado de la Habitación:</h5>
        <button class="btn btn-primary"  class="pr-2">Limpia</button>
        <button class="btn btn-warning" style="margin-left: 10px" >Bloqueada</button>
    </div>
    <div class="container pt-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="textObs" class="form-label"><b>Agregar observación</b></label>
                                    <textarea class="form-control" id="textObs"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-start">
                            <button class="btn btn-primary" id="btnCamera">
                            <i class="fa-solid fa-camera"  data-bs-toggle="modal" data-bs-target="#exampleModal"></i>
                            </button>
                            </div>
                            <div class="col text-end">
                                <button class="btn btn-sm btn-success">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <card class="mt-5 mx-auto">
                    <card-text>
                    </card-text>
                    <card id="image">
                    </card>
                </card>
            </div>
        </div>
    </div>
    </div>`
}