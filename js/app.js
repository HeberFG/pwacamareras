if (navigator.serviceWorker) {
    navigator.serviceWorker.register('/sw.js');
}

console.log("app importado");

const btnCamera = document.getElementById("btnCamera");
const btnMakePhoto = document.getElementById("btnMakePhoto");

console.log(btnCamera);

const video = document.getElementById("video");
const itemPhoto = document.getElementById("itemPhoto");

let pictures = [];

const camera = new Camera(video);

btnCamera.addEventListener("click", () => {
  console.log("Abriendo camara");
  camera.power();
});

btnMakePhoto.addEventListener("click", () => {
  console.log("Tomando foto");
  let picture = camera.takePhoto();
  console.log(picture.valueOf);
  pictures.push(picture);

  itemPhoto.innerHTML += `
  <div class="carousel-item active">
    <img  src="${
      pictures[pictures.length - 1]
    }" class="d-block w-100" alt="...">
  </div>
 `;

  //console.log(picture);
  camera.off();

  console.log(pictures.length);
});
